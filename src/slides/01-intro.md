# chaos.brussels

![Chaos logo](/chaos.png)

a [chaostreff](https://www.ccc.de/de/club/chaostreffs) in BXL

---


A Chaostreffen[1] is a meet-up for people who feel somehow connected to the Chaos Computer Club[2].

Un Chaostreffen[1] est un rendez-vous pour les personnes qui se sentent en quelque sorte liées au Chaos Computer Club[2].

Een Chaostreffen[1] is een ontmoeting voor mensen die zich op de een of andere manier verbonden voelen met de Chaos Computer Club[2].


[1] https://ccc.de/de/club/chaostreffs

---


The Chaos Computer Club is a large european association of hackers.

Le Chaos Computer Club est une grande association européenne de hackeurs.

De Chaos Computer Club is een grote Europese vereniging van hackers.

[2] https://ccc.de/

---

The activities within the CCC range from technical research and exploration on the of the technological universe through campaigns, events, policy advice, press releases and publications to the operation of anonymizing services and means of communication. The association consists of a series of decentralised local clubs and groups which organises regular events and meetings. The CCC conveys it's concern about various publication channels and always tries to talk to technically and socially interested and like-minded people. It follows the hacker-ethic[3].


[3] https://ccc.de/de/hackerethik  \\ https://en.wikipedia.org/wiki/Hacker_ethic

---

Les activités au sein de la CCC vont de la recherche technique et de l'exploration de l'univers technologique à l'exploitation de services et de moyens de communication anonymes en passant par des campagnes, des événements, des conseils stratégiques, des communiqués de presse et des publications. L'association se compose d'une série de clubs et groupes locaux décentralisés qui organisent régulièrement des événements et des réunions. Le CCC fait part de ses préoccupations au sujet des divers canaux de publication et essaie toujours de parler à des personnes intéressées sur les plans technique et social et partageant les mêmes idées. Il suit l'éthique du hacker[3].

[3] https://ccc.de/de/hackerethik  \\ https://en.wikipedia.org/wiki/Hacker_ethic

---

De activiteiten binnen de CCC variëren van technisch onderzoek en verkenning van de technologische wereld via campagnes, evenementen, beleidsadviezen, persberichten en publicaties tot de werking van de anonimisering van diensten en communicatiemiddelen. De vereniging bestaat uit een reeks gedecentraliseerde lokale clubs en groepen die regelmatig evenementen en vergaderingen organiseren. De CCC geeft uiting aan haar bezorgdheid over de verschillende publicatiekanalen en probeert altijd te praten met technisch en maatschappelijk geïnteresseerde en gelijkgestemde mensen. Het volgt de hacker-ethiek[3].


[3] https://ccc.de/de/hackerethik  \\ https://en.wikipedia.org/wiki/Hacker_ethic

---


